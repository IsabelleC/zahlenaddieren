import java.util.Scanner;
import java.util.*; //kA wo die InputMismatchException genau ist

public class Training {

    private String title;

    public Training(String title){
        this.title = title;
    }

    //todo Task 1
    public void addTwoNumbers(){

      Scanner scan = new Scanner (System.in);
      double eingabe1 = 0;
      double eingabe2 = 0;
      double ergebnis;

      eingabe1 = scan.nextDouble();
      eingabe2 = scan.nextDouble();
      ergebnis = eingabe1+eingabe2;

      System.out.println("Das Ergebnis ist:"+ergebnis+"!");

    }

    //todo Task 2
    public void swapTwoNumbers(){

        Scanner scan = new Scanner (System.in);
        double a = scan.nextDouble();
        double b = scan.nextDouble();

                a+=b;
                b=a-b;
                a-=b;

                System.out.println("a="+a);
                System.out.println("b="+b);


    }

    //todo Task 3
    public void printStairsOfNumbers(){

        int count = 1;

        for (int i=1; i<=4; i++){
            for (int j=1; j<=i; j++){
                System.out.print(count+" ");
                count++;
            }
            System.out.println();
        }
    }

    public static void main(String[] args){
        Training t1 = new Training("Here we go. Let's do Exercise 1!");
        System.out.println("Task 1: Add two Numbers (ints, floats,...)\n");
        t1.addTwoNumbers();
        System.out.println("Task 2: Swap two Numbers without using a helper variable:\n");
        t1.swapTwoNumbers();
        System.out.println("Task 3: Pretty print Numbers:\n");
        t1.printStairsOfNumbers();
    }
}
